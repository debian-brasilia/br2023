---
title: Seja um patrocinador
---

# Seja um patrocinador

Estamos buscando empresas ou entidades que queiram patrocinar a MiniDebConf
Curitiba 2018.

Se você tem interesse, ou tem contato com organizações que possam ajudar, por
favor nos envie um email brasil.mini@debconf.org

Você pode fazer o download do nosso
[plano de patrocínio](../MiniDebConf-Curitiba-2018-patrocinio.pdf).
