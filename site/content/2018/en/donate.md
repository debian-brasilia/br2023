---
title: Crowdfunding
---

# Crowfunding

You can contribute financially to help MiniDebConf Curitiba 2018 happen making
a donation of any value even if you don't plan to attend the event. Donations
will be done to the Curitiba Livre Community, the local group that is
organizing the event.

The money received will be used to pay expenses such as:

* Paying for the Auditorium employees to work on the Saturday;
* Making graphic materials for marketing of the event;
* Making conference banners for signaling in the venue;
* Coffee breaks.

<br />
<div id="widgetwci">
  <div id="crm_wid_5" class="crm-wci-widget wide">
    <div class="crm-amount-bar">
      <div id="crm_wid_5_amt_hi" class="crm-amount-high">
        R$ 3.000,00
      </div>
      <div class="crm-amount-fill" id="crm_wid_5_amt_fill">
        <div id="crm_wid_5_low" class="crm-amount-low">
          R$ 2.376,00
        </div>
      </div>
    </div>
  </div>
</div>
<br />

## Donate via Paypal

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<fieldset>
<input type="hidden" name="cmd" value="_donations">
<input type="hidden" name="business" value="brasil.mini@debconf.org">
<input type="hidden" name="lc" value="BR">
<input type="hidden" name="item_name" value="MiniDebConf Curitiba 2018">
<input type="hidden" name="no_note" value="0">
<input type="hidden" name="currency_code" value="BRL">
<input type="hidden" name="bn" value="PP-DonationsBF:btn_donateCC_LG.gif:NonHostedGuest">
<input type="submit" value="Donate with Paypal">
<img alt="" border="0" src="https://www.paypalobjects.com/pt_BR/i/scr/pixel.gif" width="1" height="1">
</fieldset>
</form>

## Donate Bitcoin

<fieldset>
Send Bitcoin to the following address: 1AMrSpQQERi8f2HjAjZd4USY9tZCre6i2P.

bitcoin://1AMrSpQQERi8f2HjAjZd4USY9tZCre6i2P

![qrcode bitcoins](/images/qrcode.jpg "qrcode bitcoins")
</fieldset>

## Donor list

We will list below the names of the donors, without values, as a way of
recognizing their contribution.

<%= File.read items['/2018/donors.txt'][:filename] %>

## Observations

* If you wish to make an anonymous donations, send an email to
  <daniel@sombra.eti.br> saying so.
* If you make a donation in bitcoins, please send an email to
  <daniel@sombra.eti.br> saying so, so we can list your name here.
