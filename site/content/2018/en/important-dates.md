---
title: Important dates
---

# Important dates

# January 2018

* 31st (Wednesday, 23:59: Deadline for submiting
  [proposals](../call-for-proposals).

# February 2018

* 11th: (Sunday): announcement of accepted proposals

# March 2018

* 10 (Saturday): Debian Women Meeting in Curitiba.

# April 2018

* 11th and 12th: (Wednesday, Thursday): MiniDebCamp
* 13th adn 14th (Friday, Saturday): MiniDebConf
* 15th: (Sunday): contraternization lunch.


