---
title: Call for proposals
---

# Call for proposals

The call for proposals for the MiniDebConf Curitiba 2018 is open.

# Topics

In principle, any topic related to free software is in the scope of the
conference. However, topics related to Debian will be given priority, and among
those, topics related to contributing to Debian will be given priority.

## Diversity

Our MiniDebConf is a conference committed to diversity. We would like very much
to receive submissions from people that are part of groups underrepresented in
the free software community that would like to speak about Debian. If you are
part of one of those groups, please send us a proposal. If you know someone
from those groups, please suggest that they submit.

If you would like to talk about Debian but is unsure about a specific topic,
[contact us](../contact/) and we can try to help you.

# Types of activities

You can submit 4 types of proposals:

* Lightning talks (5 min)
* Panels
* Talks
* Workshop (3 hours)

For panels and talks, you can choose the duration among:

* 30 minutes
* 1 hour
* 1 hour 30 minutes
* 2 hours

## Dates

* Submission deadline: January 31st, 2018.
* Notification of selection results to proponents: until February 11th, 2018.

## Submission

To submit your proposal use the form below (the first field is a language
selector, switch to English and the rest of the form will be in English):

<http://pesquisa.softwarelivre.org/index.php/574335>
