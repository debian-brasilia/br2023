---
title: Voluntários
---

# Voluntários

Como o MiniDebConf Curitiba é um evento organizado por voluntários, sua
qualidade depende fortemente do nosso esforço, portanto, **toda ajuda é
bem-vinda!**  Se você está se perguntando como e com o que exatamente você pode
ajudar, aqui está a lista de algumas das atividades que você pode nos dar uma
mão:

 - **Fotografia**: Leve sua câmera fotográfica e registre as atividades

 - **Conteúdo**: Durante o evento, poste conteúdo nas redes sociais nos perfis
 do Debian Brasil e Curitiba Livre (você terá acesso a esses perfis de redes
 sociais)

 - **Secretaria**: Ajude no credenciamento dos participantes no decorrer do
 evento

 - **Transmissão das Palestras**: Auxiliar na filmagem/transmissão das
 palestras. Não se preocupe! Estaremos lá para auxiliá-los :)

 - **Auditório**: Dê suporte aos palestrantes durante o evento, ajudando na
 utilização de microfones e projetores, cronometrando tempo para término da
 apresentação, e o que mais for preciso.

<br>
<span style="font-size: 20px">Se interessou?! Quer nos ajudar?! Se registre neste
[formulário](http://pesquisa.softwarelivre.org/index.php/992958?lang=pt-BR) e
entraremos em contato com você!</span>
