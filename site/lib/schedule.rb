require 'cgi'
require 'rdiscount'
include Nanoc::Helpers::Rendering

class Activity < Struct.new(:title, :name, :bio, :description, :webm, :youtube)
  def initialize(data)
    self.class.members.each do |field|
      value = data[field.to_s]
      self.send("#{field}=", value)
    end
  end

  def to_html
    d = CGI.escapeHTML(RDiscount.new(description.to_s).to_html)
    t = CGI.escapeHTML(title.to_s)
    b = CGI.escapeHTML(RDiscount.new(bio.to_s).to_html)
    s = CGI.escapeHTML(Array(name).join(', '))
    [
      '<p><strong><a class="title" data-container="body" data-toggle="popover" data-html="true" data-placement="top" title="%s" data-content="%s">%s</a></strong></p>' % [t, d, t],
      '<p><a class="speaker" data-container="body" data-toggle="popover" data-html="true" data-placement="top" title="%s" data-content="%s">%s</a></p>' % [s, b, s],
    ].join("\n\n")
  end

end

def schedule_data(year)
  @schedule_data ||= {}
  @schedule_data[year] ||=
    begin
      all = {}
      YAML.load_file('content/%d/schedule-data.yml' % year)[:talks].each do |key,data|
        all[key.to_sym] = Activity.new(data)
      end
      all
    end
end

def schedule(key)
  activity = schedule_data(get_year).fetch(key)
  html = activity.to_html
  if activity.webm
    html << '<p><a href="%s"><i class="fa fa-play-circle"></i> %s</a></p>' % [activity.webm, _('Watch from the Debian video repository (WebM format)')]
  end

  if activity.youtube
    html << '<p><a href="%s"><i class="fa fa-youtube-play"></i> %s</a></p>' % [activity.youtube, _('Watch on Youtube')]
  end

  html
end

alias :s :schedule
